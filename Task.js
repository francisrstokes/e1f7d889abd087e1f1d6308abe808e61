const daggy = require('daggy');
const {curry, compose} = require('ramda');

const fork = curry((err, succ, T) => {
  return T.fork(err, succ)
});

const Task = daggy.tagged('Task', ['__value']);

Task.of = function (x) {
  return Task((reject, resolve) => resolve(x));
};

Task.prototype.map = function (fn) {
  return Task.of((reject, resolve) => {
    this.fork(reject, compose(resolve, fn));
  });
};

Task.prototype.chain = function (fn) {
  return Task.of((reject, resolve) => {
    this.fork(reject, compose(fork(reject, resolve), fn));
  });
};

Task.prototype.fork = curry(function (errorFn, successFn) {
  return this.__value(errorFn, successFn);
});

Task.prototype.ap = function (T) {
  return T.chain(f => this.map(f));
};

module.exports = Task;