const daggy = require('daggy');

const Maybe = daggy.taggedSum('Maybe', { Just: ['__value'], Nothing: [] });
const { Just, Nothing } = Maybe;
Maybe.of = (x) => Just(x);

Maybe.prototype.map = function (fn) {
  return this.cata({
    Just: (x) => Maybe.of(fn(x)),
    Nothing: Maybe.Nothing
  });
};

Maybe.prototype.join = function () {
  return this.cata({
    Just: () => this.__value,
    Nothing: () => Maybe.Nothing
  });
};

Maybe.prototype.chain = function (fn) {
  return this.map(fn).join();
};

Maybe.prototype.ap = function (container) {
  return container.cata({
    Just: (x) => this.map(x),
    Nothing: () => Maybe.Nothing
  });
};

module.exports = Maybe;