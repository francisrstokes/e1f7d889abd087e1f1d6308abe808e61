const daggy = require('daggy');
const {identity} = require('ramda');

const Either = daggy.taggedSum('Either', {
  Left: ['__value'],
  Right: ['__value'],
});

Either.of = Either.Right;

Either.prototype.map = function (fn) {
  return this.cata({
    Left: () => this,
    Right: (x) => Either.Right(fn(x))
  })
};

Either.prototype.join = function () {
  return this.chain(identity);
};

Either.prototype.chain = function (fn) {
  return this.cata({
    Left: () => this,
    Right: fn // Might be a type error 🙈
  });
};

Either.prototype.ap = function (container) {
  return this.map(container.__value);
};

module.exports = Either;